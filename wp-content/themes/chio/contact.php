<?php
/*
Template Name: Контакты
*/
get_header()
?>

<main>
	<ul class="breadcrumbs">
		<li><a href="/"> Главная</a></li>
		<li>Вакансии в Чио Чио</li>
	</ul>

	<section class="vacancies">
		<div class="container">
			<div class="vacancies__wrapper">
				<h1 class="section-name">Вакансии в Чио Чио</h1>
				<p class="vacancies__description">
					Своим содрудникам федеральная <br />
					сеть парикмахерских Чио Чио <br />
					предоставляет возможность <br />обучения, достойную <br />
					зарплату, комфортные <br />условия труда, <br />
					и высокий <br />
					уровень <br />корпоративной <br />
					культуры.
				</p>
				<a href="#" class="buttom">Отправить резюме</a>
				<div class="vacancies__description-img">
					<img src="/wp-content/themes/chio/assets/images/vac.png" alt="" />
				</div>
			</div>

			<a href="#" class="buttom buttom-mobail">Отправить резюме</a>
		</div>
	</section>
</main>


<?php
get_footer()
?>