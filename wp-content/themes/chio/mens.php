<?php
/*
Template Name: Мужчинам
*/
get_header()
?>

<main>
	<ul class="breadcrumbs">
		<li><a href="/"> Главная</a></li>
		<li>Мужчинам</li>
	</ul>

	<section class="mens">
		<div class="container">
			<h2 class="section-name">Мужчинам</h2>
			<div class="card__wrapper">
				<?php if (have_rows('баннеры')) : ?>
					<?php while (have_rows('баннеры')) : the_row();
						$image = get_sub_field('фото');
						$content = get_sub_field('заголовок');
						$link = get_sub_field('описание');

					?>
						<div class="card">
							<h2 class="card-name">
								<?php echo $content; ?>
							</h2>
							<p class="card-description">
								<?php echo $link; ?>
							</p>
							<div class="card-images">
								<img src="	<?php echo $image; ?>" alt="Мужчины" />
							</div>
						</div>
					<?php endwhile; ?>
				<?php endif; ?>
			</div>
		</div>
	</section>

	<section class="price">
		<div class="container">
			<?php if (have_rows('разделы')) : ?>
				<?php while (have_rows('разделы')) : the_row();
					$content = get_sub_field('заголовок');
				?>

					<h2 class="price__name">
						<?php echo $content; ?>
					</h2>
					<?php if (have_rows('прайс')) : ?>
						<?php while (have_rows('прайс')) : the_row();
							$content = get_sub_field('название');
							$link = get_sub_field('цена');

						?>
							<div class="table">
								<div class="line">
									<p><?php echo $content; ?></p>
									<p><?php echo $link; ?></p>
								</div>
							</div>
						<?php endwhile; ?>
					<?php endif; ?>
				<?php endwhile; ?>
			<?php endif; ?>
			<button class="buttom" onclick="window.location.href='/zapisatsja';">Записаться</button>
		</div>
	</section>
</main>


<?php
get_footer()
?>