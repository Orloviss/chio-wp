<?php
/*
 Template name: Акция
 Template post type: post, page
*/
get_header()
?>

<main>
	<ul class="breadcrumbs">
		<li><a href="/"> Главная</a></li>
		<li>Акции</li>
	</ul>

	<section class="baner ">
		<div class="container">
			<div class="baner__wrapper">
				<div class="baner__content">
					<h1 class="baner__headtext">
						<?php the_title(); ?>
					</h1>
				</div>
				<img class="baner__image" src="../assets/images/clock.svg" alt="" />
			</div>
		</div>
	</section>

	<section class="promotion__card-mob">
		<div class="container">
			<div class="promotion__wrapper-card">
				<div class="promotion__wrapper-card-name">
					<?php the_title(); ?>
				</div>
				<img class="promotion__wrapper-card__img-star" src="../assets/images/shit.png" alt="" />
			</div>
		</div>
	</section>

	<section class="promotion-description">
		<div class="container">
			<?php the_content() ?>

			<button class="buttom" onclick="window.location.href='/zapisatsja';">Записаться</button>
		</div>
	</section>
</main>


<?php
get_footer()
?>