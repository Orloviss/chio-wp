const burger = document.querySelector('.burger-menu');
const navigationMobail = document.querySelector('.navigation-mobail');
const navigationLi = document.querySelector('.navigation-mobail__li');
const navigationLiTwo = document.querySelector('.navigation-mobail__list-two');

burger.addEventListener('click', () => {
  burger.classList.toggle('_active');
  navigationMobail.classList.toggle('_active');
});

navigationLi.addEventListener('click', () => {
  navigationLi.classList.toggle('_active');
  navigationLiTwo.classList.toggle('_active');
});

const singUp = document.querySelector('.singUp');
const step1 = document.querySelector('.step1');
const login = document.querySelector('.login');
const btnEnter = document.querySelector('.step1 .buttom');
const btnCode = document.querySelector('.step3 .buttom');
const step2 = document.querySelector('.step2');
const step3 = document.querySelector('.step3');
const step4 = document.querySelector('.step4');
const close = document.querySelectorAll('.close');

close.forEach((cl) => {
  cl.addEventListener('click', (e) => {
    cl.closest('.pop-up').style.display = 'none';
    document.body.classList.remove('_lock');
  });
});
singUp.addEventListener('click', () => {
  step1.style.display = 'flex';
  document.body.classList.add('_lock');
});
login.addEventListener('click', () => {
  step1.style.display = 'flex';
  document.body.classList.add('_lock');
});
btnEnter.addEventListener('click', () => {
  step1.style.display = 'none';
  step2.style.display = 'flex';
});
btnEnter.addEventListener('click', () => {
  step2.style.display = 'none';
  step3.style.display = 'flex';
});




jQuery(document).ready(function ($) {
  const search_input = $(".search-form__input");
  const search_results = $(".ajax-search");

  search_input.keyup(function (event) {
    let search_value = $(this).val();

    if (search_value.length > 2) { // кол-во символов
      $.ajax({
        url: "/wp-admin/admin-ajax.php",
        type: "POST",
        data: {
          "action": "ajax_search", // functions.php
          "term": search_value
        },
        success: function (results) {
          search_results.fadeIn(200).html(results);
        }
      });
    } else {
      search_results.fadeOut(200);
    }
  });

  // Block Enter key when cursor is in the input field
  search_input.keydown(function (event) {
    if (event.keyCode === 13) {
      event.preventDefault();
      return false;
    }
  });

  // Закрытие поиска при клике вне его
  $(document).mouseup(function (e) {
    if (
      (search_input.has(e.target).length === 0) &&
      (search_results.has(e.target).length === 0)
    ) {
      search_results.fadeOut(200);
    };
  });
});

