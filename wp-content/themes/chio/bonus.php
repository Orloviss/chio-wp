<?php
/*
Template Name: Бонусная программа
*/
get_header()
?>
<main>
	<ul class="breadcrumbs">
		<li><a href="/"> Главная</a></li>
		<li>Бонусная программа</li>
	</ul>

	<section class="bonus bonus-page">
		<div class="container">
			<div class="bonus__wrapper">
				<img class="bonus__image" src="/wp-content/themes/chio/assets/images/bonus.png" alt="" />
				<img class="bonus__image-mob" src="/wp-content/themes/chio/assets/images/bonus-page-m.png" alt="" />
				<div class="bonus__content">
					<h2 class="bonus__content-headtext">
						Бонусная программа <br />
						Чио Чио
					</h2>
				</div>
			</div>
		</div>
	</section>

	<section class="bonus-page__description">
		<div class="container">
			<p class="bonus-page__description-text">
				<?php the_field('описание_системы'); ?>	
			</p>
			<button class="buttom" onclick="window.location.href='/zapisatsja';">Записаться</button>
		</div>
	</section>
</main>
<?php
get_footer()
?>