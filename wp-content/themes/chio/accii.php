<?php
/*
Template Name: Акции
*/
get_header()
?>


<main>
	<ul class="breadcrumbs">
		<li><a href="/"> Главная</a></li>
		<li>Акции</li>
	</ul>

	<section class="baner">
		<div class="container">
			<div class="baner__wrapper">
				<div class="baner__content">
					<h1 class="baner__headtext">
						Счастливые <br />
						часы
					</h1>
					<button class="buttom">Записаться</button>
				</div>
				<img class="baner__image" src="/wp-content/themes/chio/assets/images/clock.svg" alt="" />
			</div>
		</div>
	</section>

	<section class="all-promotion">
		<div class="container">
			<div class="all-promotion__wrapper">
				<div class="all-promotion__card">
					<h2 class="all-promotion__card-name">
						Стрижка ветеранам бесплатно
					</h2>
				</div>

				<div class="all-promotion__card">
					<h2 class="all-promotion__card-name">
						3в1 <br />
						Комбо <br />
						для женщин
					</h2>
				</div>

				<div class="all-promotion__card">
					<h2 class="all-promotion__card-name">
						3в1 <br />
						Комбо <br />
						для женщин
					</h2>
				</div>

				<div class="all-promotion__card">
					<h2 class="all-promotion__card-name">
						Стрижка ветеранам бесплатно
					</h2>
				</div>
			</div>
		</div>
	</section>

	<section class="all-promotion-mob">
		<div class="container">
			<h2 class="section-name">Все акции</h2>
			<div class="promotion__wrapper">
				<a href="#" class="promotion__wrapper-card">
					<div>
						Скидка на все <br />
						услуги 20%
					</div>
				</a>
				<a href="#" class="promotion__wrapper-card">
					<div>Чиокоин</div>
				</a>
				<a href="#" class="promotion__wrapper-card card-veterans">
					<div class="promotion__wrapper-card-name">
						Стрижка ветеранам <br />
						бесплатно
					</div>
					<p class="promotion__wrapper-card-description">Всегда</p>
					<img src="/wp-content/themes/chio/assets/images/star-card.svg" alt="звезда" class="promotion__wrapper-card__img-star" />
				</a>
				<a href="#" class="promotion__wrapper-card card-clock">
					<div class="promotion__wrapper-card-name">
						Счастливые <br />
						часы
					</div>
					<p class="promotion__wrapper-card-description-clock">
						С 11:00 до 13:00 скидка <br />
						на стрижки 20%
					</p>
					<img class="promotion__wrapper-card__img-clock" src="/wp-content/themes/chio/assets/images/clock.svg" alt="часы" />
				</a>
				<a href="#" class="promotion__wrapper-card">
					<div class="promotion__wrapper-card-name">
						Скидка на все <br />
						услуги 20%
					</div>
					<img class="promotion__wrapper-card__img-star" src="/wp-content/themes/chio/assets/images/shit.png" alt="" />
				</a>
				<button class="buttom">Записаться</button>
			</div>
		</div>
	</section>
</main>


<?php
get_footer()
?>