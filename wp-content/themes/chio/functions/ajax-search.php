<?php

// ajax поиск по сайту
add_action("wp_ajax_nopriv_ajax_search", "ajax_search");
add_action("wp_ajax_ajax_search", "ajax_search");
function ajax_search()
{
	$args = array(
		"post_type"      => "page", // Set the post type to "page" to search only pages
		"post_status"    => "publish",
		"order"          => "DESC",
		"orderby"        => "date",
		"s"              => $_POST["term"],
		"posts_per_page" => -1,
		"meta_query"     => array(
			array(
				"key"     => "_wp_page_template", // Exclude pages with specific templates if needed
				"value"   => array("template-name.php", "another-template.php"),
				"compare" => "NOT IN"
			)
		)
	);
	$query = new WP_Query($args);

	if ($query->have_posts()) {
		while ($query->have_posts()) : $query->the_post();
			echo '<li class="ajax-search__item">';
			echo ' <a href="';
			the_permalink();
			echo '">';
			the_title();
			echo '</a>';
			echo '</li>';
			echo '<style>ul.ajax-search {
    padding: 20px;}</style>';
		endwhile;
	} else {
		echo '<li class="ajax-search__item">';
		echo ' <a>';
		echo "Ничего не найдено";
		echo '</a>';
		echo '</li>';
		echo '<style>ul.ajax-search {
    padding: 20px;}</style>';
	}
	exit;
}

// Add the following filters to enable searching by content

function cf_search_join($join)
{
	global $wpdb;
	if (is_search()) {
		$join .= ' LEFT JOIN ' . $wpdb->postmeta . ' ON ' . $wpdb->posts . '.ID = ' . $wpdb->postmeta . '.post_id ';
	}
	return $join;
}
add_filter('posts_join', 'cf_search_join');


function cf_search_where($where)
{
	global $pagenow, $wpdb;
	if (is_search()) {
		$where = preg_replace(
			"/\(\s*" . $wpdb->posts . ".post_title\s+LIKE\s*(\'[^\']+\')\s*\)/",
			"(" . $wpdb->posts . ".post_title LIKE $1) OR (" . $wpdb->posts . ".post_content LIKE $1) OR (" . $wpdb->postmeta . ".meta_value LIKE $1)",
			$where
		);
	}
	return $where;
}
add_filter('posts_where', 'cf_search_where');


function cf_search_distinct($where)
{
	global $wpdb;
	if (is_search()) {
		return "DISTINCT";
	}
	return $where;
}
add_filter('posts_distinct', 'cf_search_distinct');
