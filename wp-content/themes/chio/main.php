<?php
/*
Template Name: Главная
*/
get_header()
?>

<main>
  <section class="start-screen">
    <div class="container">
      <div class="start-screen__wrapper">
        <div class="start-screen__content">
          <h1 class="start-screen__content-headtext">
        <?php the_field('текст_главного_блока'); ?>		  
          </h1>
          <button class="buttom" onclick="window.location.href='/zapisatsja';">Записаться</button>
        </div>

        <div class="start-screen__images">
          <div class="start-screen__images-wrapper">
            <img src="/wp-content/themes/chio/assets/images/start-screen.png" alt="главная картинка" />
          </div>
        </div>

        <button class="buttom" onclick="window.location.href='/zapisatsja';">Записаться</button>
      </div>
    </div>
  </section>

  <section class="choice">
    <div class="container">
      <div class="choice__wrapper">
        <a class="choice__card" href="/muzhchinam">
          <h2 class="choice__card-name">Мужчинам</h2>
          <div class="choice__card-images">
            <div class="choice__card-images-wrapper">
              <img src="/wp-content/themes/chio/assets/images/men.png" alt="Мужчинам" />
            </div>
          </div>
        </a>

        <a class="choice__card" href="/zhenshhinam">
          <h2 class="choice__card-name">Женщинам</h2>
          <div class="choice__card-images">
            <div class="choice__card-images-wrapper">
              <img src="/wp-content/themes/chio/assets/images/women.png" alt="Женщинам" />
            </div>
          </div>
        </a>

        <a class="choice__card" href="/detjam">
          <h2 class="choice__card-name">Детям</h2>
          <div class="choice__card-images">
            <div class="choice__card-images-wrapper">
              <img src="/wp-content/themes/chio/assets/images/children.png" alt="Детям" />
            </div>
          </div>
        </a>
      </div>
    </div>
  </section>

  <section class="masters">
    <div class="container">
      <h2 class="section-name"><?php the_field('заголовок_списка'); ?>		</h2>
      <ul class="masters__list">
        <li class="masters__li"><?php the_field('пункт_списка_один'); ?>		 </li>
        <li class="masters__li"><?php the_field('пункт_списка_два'); ?>		 </li>
        <li class="masters__li"><?php the_field('пункт_списка_три'); ?>		 </li>
        <li class="masters__li"><?php the_field('пункт_списка_четыре'); ?>		 </li>
      </ul>
    </div>
  </section>

  <section class="promotion">
    <div class="container">
      <div class="section-name__wrapper">
        <h2 class="section-name">Акции</h2>
        <a class="btn__all-promotion" href="/akcii">Все акции</a>
      </div>
      <div class="promotion__wrapper">
        <a href="./pages/promotionPage.html" class="promotion__wrapper-card">
          <div>
            Скидка на все <br />
            услуги 20%
          </div>
        </a>
        <a href="#" class="promotion__wrapper-card">
          <div>Чиокоин</div>
        </a>
        <a href="#" class="promotion__wrapper-card card-veterans">
          <div class="promotion__wrapper-card-name">
            Стрижка ветеранам <br />
            бесплатно
          </div>
          <p class="promotion__wrapper-card-description">Всегда</p>
          <img src="/wp-content/themes/chio/assets/images/star-card.svg" alt="звезда" class="promotion__wrapper-card__img-star" />
        </a>
        <a href="#" class="promotion__wrapper-card card-clock">
          <div>
            Счастливые <br />
            часы
          </div>
          <p class="promotion__wrapper-card-description-clock">
            С 11:00 до 13:00 скидка <br />
            на стрижки 20%
          </p>
          <img class="promotion__wrapper-card__img-clock" src="/wp-content/themes/chio/assets/images/clock.svg" alt="часы" />
        </a>
        <a href="#" class="promotion__wrapper-card">
          <div>
            Стрижка <br />
            ветеранам <br />
            бесплатно
          </div>
        </a>
      </div>

      <a class="btn__all-promotion btn__all-promotion-mabail" href="/akcii">Все акции</a>
    </div>
  </section>

  <section class="about">
    <div class="container">
      <h2 class="section-name">Почему Чио Чио</h2>
      <div class="about__wrapper">
        <div class="about__item">
          <img class="about__item-img-dec" src="/wp-content/themes/chio/assets/images/cup.png" alt="кубок" />
          <img class="about__item-img-mob" src="/wp-content/themes/chio/assets/images/cup-mob.png" alt="кубок" />
          <p class="about__item-description">
            Сеть <br />
            парикмахерских <br />
            №1 в России
          </p>
        </div>

        <div class="about__item">
          <img class="about__item-img-dec" src="/wp-content/themes/chio/assets/images/closet.png" alt="кубок" />
          <img class="about__item-img-mob" src="/wp-content/themes/chio/assets/images/closet-mob.png" alt="кубок" />
          <p class="about__item-description">
            Японские <br />
            технологии <br />
            стрижек
          </p>
        </div>

        <div class="about__item">
          <img class="about__item-img-dec" src="/wp-content/themes/chio/assets/images/sterility.png" alt="кубок" />
          <img class="about__item-img-mob" src="/wp-content/themes/chio/assets/images/sterility-mob.png" alt="кубок" />
          <p class="about__item-description">
            Стерильность <br />
            инструментов, <br />
            чистота в зале
          </p>
        </div>

        <div class="about__item">
          <img class="about__item-img-dec" src="/wp-content/themes/chio/assets/images/men-smail.png" alt="кубок" />
          <img class="about__item-img-mob" src="/wp-content/themes/chio/assets/images/men-mob.png" alt="кубок" />
          <p class="about__item-description">
            Качество <br />без наценок
          </p>
        </div>
      </div>
    </div>
  </section>

  <section class="bonus">
    <div class="container">
      <div class="bonus__wrapper">
        <img class="bonus__image" src="/wp-content/themes/chio/assets/images/bonus.png" alt="" />
        <img class="bonus__image-mob" src="/wp-content/themes/chio/assets/images/bonus-mob.png" alt="" />
        <div class="bonus__content">
          <h2 class="bonus__content-headtext">
            Бонусная программа Чио Чио
          </h2>
          <p class="bonus__content-subtext">
         <?php the_field('текст_бонусной_программы'); ?>	
          </p>
        </div>
        <a href="/bonusnaja-programma" class="buttom">Подробнее</a>
      </div>
    </div>
  </section>

  <section class="achievements">
    <div class="container">
      <div class="achievements__wrapper">
        <div class="achievements__images">
          <img src="/wp-content/themes/chio/assets/images/one.svg" alt="" />
        </div>
        <p class="achievements__text">
          Сеть парикмахерских <br />
          №1 в России
        </p>
        <p class="achievements__text-mob">
          Чио Чио сеть парикмахерских №1 в России
        </p>
        <p class="achievements__subtext">
         <?php the_field('количество_точек'); ?>	
          <a href="/franshiza"> Подробнее </a>
        </p>
      </div>
      <button class="achievements__btn" onclick="window.location.href='/franshiza';">Подробнее о компании</button>
    </div>
  </section>

  <section class="franchise">
    <div class="container">
      <div class="franchise__wrapper">
        <img class="franchise__images" src="/wp-content/themes/chio/assets/images/franchise-img.png" alt="" />
        <div class="franchise__content">
          <h3 class="franchise__content__headtext">Франшиза Чио Чио</h3>
          <p class="franchise__content__subtext">
            Стань партнером самой крупной сети парикмахерских в России
          </p>
          <button class="buttom" onclick="window.location.href='/franshiza';">Подробнее</button>
        </div>
        <img class="franchise__images-right" src="/wp-content/themes/chio/assets/images/franchise-img2.svg" alt="" />
      </div>
    </div>
  </section>

  <section class="offers">
    <div class="container">
      <div class="offers__wrapper">
        <div class="offers__card">
          <div class="offers__card-wrapper">
            <h3 class="offers__card__name">
              Стань совладельцем компании Чио Чио
            </h3>
            <p class="offers__card__description">
              Инвестируй в сеть №1 по версии РБК и заработай 121% <br />
              <a href="/investicii"> Подробнее </a>
            </p>
            <img class="offers__card__image" src="/wp-content/themes/chio/assets/images/offers-card.png" alt="" />
          </div>
        </div>

        <div class="offers__card">
          <div class="offers__card-wrapper">
            <h3 class="offers__card__name">
              Стабильная работа в Чио Чио
            </h3>
            <p class="offers__card__description">
              В самой крупной сети парикмахерских в России
              <br />
              <a href="/vakansii"> Подробнее </a>
            </p>
            <img class="offers__card__image offers__card__image-two" src="/wp-content/themes/chio/assets/images/offers-card-two.png" alt="" />
          </div>
        </div>

        <div class="offers__card offers__card-mob">
          <div class="offers__card-wrapper">
            <h3 class="offers__card__name">Работайте в Чио Чио</h3>
            <p class="offers__card__description">
              В самой крупной сети парикмахерских в России

              <br />
              <a href="/vakansii"> Подробнее </a>
            </p>
            <img class="offers__card__image offers__card__image-two" src="/wp-content/themes/chio/assets/images/offers-card-two.png" alt="" />
          </div>
        </div>
      </div>
    </div>
  </section>

  <section class="investments">
    <div class="container">
      <div class="investments__contant">
        <h2 class="section-name">Стань совладельцем компании Чио Чио </h2>
        <p class="investments__contant-subtext">
          Инвестируй в долю сети парикмахерских №1 по версии РБК и
          заработай 121% в первый год
        </p>
      </div>
      <button class="achievements__btn" onclick="window.location.href='/investicii';">Подробнее об инвестициях</button>
    </div>
  </section>

  <section class="franchise__mob">
    <div class="container">
      <div class="franchise__mob__wrapper">
        <img class="franchise__mob__images" src="/wp-content/themes/chio/assets/images/franchise-mob.png" alt="" />
        <div class="franchise__mob__contant">
          <h2 class="franchise__mob__contant-name">Франшиза Чио Чио</h2>
          <p class="franchise__mob__contant-subtext">
            Стань партнером самой крупной сети парикмахерских в России
            <br />
            <a href="/franshiza"> Узнать подробности </a>
          </p>
        </div>
      </div>
    </div>
  </section>
</main>

<?php
get_footer()
?>