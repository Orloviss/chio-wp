<?php

/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package chio
 */

?>


<footer class="footer" id="footer">
	<div class="container">
		<div class="footer__wrapper">
			<nav class="footer__navigate">
				<?php
				wp_nav_menu(array(
					'theme_location' => 'primary',
					'menu_class' => 'footer__navigation-list',
				));
				?>

			</nav>

			<div class="footer__contacts">
				<a class="footer__phone" href="tel:88006006516">8 800 600-65-16</a>

				<div class="footer__social">
					<a href="#" class="insta">
						<img src="/wp-content/themes/chio/assets/images/social/insta.svg" alt="instagram" />
					</a>
					<a href="#" class="vk">
						<img src="/wp-content/themes/chio/assets/images/social/vk.svg" alt="vk" />
					</a>
					<a href="#">
						<img src="/wp-content/themes/chio/assets/images/social/tg.svg" alt="telegram" />
					</a>
					<a href="#">
						<img src="/wp-content/themes/chio/assets/images/social/whats.svg" alt="whats" />
					</a>
					<a href="#">
						<img src="/wp-content/themes/chio/assets/images/social/viber.svg" alt="viber" />
					</a>
				</div>
			</div>
		</div>
	</div>
</footer>

<div class="pop-up step1">
	<div class="container">
		<div class="pop-up__wrapper">
			<h3 class="pop-up__headtext">Личный кабинет</h3>
			<button class="buttom">Войти</button>
			<p class="pop-up__politics">
				Нажимая войти вы соглашаетесь с
				<a href="#">
					политикой <br />
					обработки персональных данных
				</a>
			</p>

			<div class="close"></div>
		</div>
	</div>
</div>

<div class="pop-up step2">
	<div class="container">
		<form class="pop-up__wrapper">
			<h3 class="pop-up__headtext">Вход в личный кабинет</h3>
			<div class="pop-up__wrapper-inputs">
				<input type="text" placeholder="Укажите ваш телефон" />
				<input type="text" placeholder="Ваше имя" />
			</div>
			<button class="buttom">Войти</button>
			<div class="close"></div>
		</form>
	</div>
</div>

<div class="pop-up step3">
	<div class="container">
		<form class="pop-up__wrapper">
			<h3 class="pop-up__headtext">Вход в личный кабинет</h3>
			<div class="pop-up__wrapper-inputs">
				<input type="text" placeholder="Введите номер из СМС" />
			</div>
			<p class="pop-up__politics">Выслать код повторно через 03:02</p>
			<div class="close"></div>
		</form>
	</div>
</div>

<div class="pop-up step4">
	<div class="container">
		<div class="pop-up__wrapper">
			<h3 class="pop-up__headtext">
				Спасибо! <br />
				Ваша запись сформирована.<br />Вам придет СМС с подтверждением
			</h3>
			<a href="/" class="buttom">На главную</a>

			<div class="close"></div>
		</div>
	</div>
</div>
</div>


<?php wp_footer(); ?>

</body>

</html>