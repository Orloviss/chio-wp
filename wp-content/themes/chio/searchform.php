<form class="search-form" role="search" method="get" id="searchform" action="<?php echo home_url('/') ?>">
	<input class="search-form__input" type="text" value="<?php echo get_search_query() ?>" name="s" id="s" placeholder="" autocomplete="off" />

	<ul class="ajax-search"></ul>
</form>