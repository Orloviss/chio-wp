<?php
/*
Template Name: Вакансии
*/
get_header()
?>

<main>
	<ul class="breadcrumbs">
		<li><a href="/"> Главная</a></li>
		<li>Вакансии в Чио Чио</li>
	</ul>

	<section class="vacancies">
		<div class="container">
			<div class="vacancies__wrapper">
				<h1 class="section-name">Вакансии в Чио Чио</h1>
				<p class="vacancies__description">
							<?php the_field('текст_вакансий'); ?>		
				</p>
				<a href="#" class="buttom">Отправить резюме</a>
				<div class="vacancies__description-img">
					<img src="/wp-content/themes/chio/assets/images/vac.png" alt="" />
				</div>
			</div>

			<a href="#" class="buttom buttom-mobail">Отправить резюме</a>
		</div>
	</section>
</main>


<?php
get_footer()
?>