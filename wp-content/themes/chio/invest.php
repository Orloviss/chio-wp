<?php
/*
Template Name: Инвестиционная программа
*/
get_header()
?>
<main>
	<ul class="breadcrumbs">
		<li><a href="/"> Главная</a></li>
		<li>Бонусная программа</li>
	</ul>

	<section class="bonus bonus-page investments-page">
		<div class="container">
			<div class="bonus__baner bonus-baner-investments">
				<h1 class="bonus__content-headtext">
					Инвестиции <br />
					в Чио Чио
				</h1>
				<img class="bonus-baner-investments-img" src="../assets/images/offers-card.png" alt="" />
			</div>
		</div>
	</section>

	<section class="bonus-page__description investments-page_description">
		<div class="container">
			<p class="bonus-page__description-text">
						<?php the_field('описание_программы'); ?>	
			</p>
			<button class="buttom" onclick="window.location.href='			<?php the_field('ссылка_на_франшизу'); ?>';">Узнать подробнее</button>
		</div>
	</section>
</main>
<?php
get_footer()
?>