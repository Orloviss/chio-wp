<?php
/*
Template Name: Франшиза
*/
get_header()
?>
<main>
	<ul class="breadcrumbs">
		<li><a href="/"> Главная</a></li>
		<li>Франшиза</li>
	</ul>

	<section class="franchise-baner">
		<div class="container">
			<div class="franchise-baner__wrapper">
				<img class="franchise-baner__img-left" src="<?php the_field('первое_фото'); ?>" alt="" />
				<img class="franchise-baner__img-right" src="<?php the_field('второе_фото'); ?>" alt="" />
			</div>
		</div>
	</section>

	<section class="promotion-description">
		<div class="container">
			<h2 class="section-name">О франшизе</h2>
			<p class="promotion-description__sale">
				<?php the_field('описание_франшизы'); ?>
			</p>
			<div class="clients">
				<h3 class="clients__name">О нас пишут в СМИ</h3>
				<div class="clients__wrapper">
					<div class="clients__logo">
						<img src="/wp-content/themes/chio/assets/images/clients/rbk.svg" alt="" />
					</div>
					<div class="clients__logo">
						<img src="/wp-content/themes/chio/assets/images/clients/1.svg" alt="" />
					</div>
					<div class="clients__logo">
						<img src="/wp-content/themes/chio/assets/images/clients/vk.svg" alt="" />
					</div>
					<div class="clients__logo">
						<img src="/wp-content/themes/chio/assets/images/clients/ru.svg" alt="" />
					</div>
					<div class="clients__logo">
						<img src="/wp-content/themes/chio/assets/images/clients/ntv.svg" alt="" />
					</div>
					<div class="clients__logo">
						<img src="/wp-content/themes/chio/assets/images/clients/tin.svg" alt="" />
					</div>
				</div>
			</div>
			<p class="promotion-description__date">
				<?php the_field('второе_описание_франшизы'); ?>
			</p>
		</div>
	</section>

	<section class="statistics">
		<div class="container">
			<div class="statistics__wrapper">
				<div class="statistics__place">
					<div class="statistics__place-open">
						<div class="statistics__place-open__img">
							<img src="/wp-content/themes/chio/assets/images/chair.svg" alt="" />
						</div>
						<h3 class="statistics__place-open__text">
							<span>	<?php the_field('число_точек'); ?></span> <br />
							точек
						</h3>
					</div>
					<h3 class="statistics__place-text">
						<?php the_field('рабочие_точки'); ?>	
					</h3>
				</div>

				<div class="statistics__employees">
					<h3 class="statistics__employees-text">
					<?php the_field('вторые_точки'); ?>		
					</h3>

					<div class="statistics__employees-img">
						<img src="/wp-content/themes/chio/assets/images/scissors.svg" alt="">
					</div>
					<h3 class="statistics__employees-subtext">
						<span>	<?php the_field('парикмахеры'); ?>	</span><br />парикмахеров
					</h3>
				</div>

				<h3 class="statistics__clients">
					<span>	<?php the_field('постоянные_клиенты'); ?>	</span><br />постоянных клиентов
				</h3>
			</div>
			<a target="_blank" href="	<?php the_field('сайт_франшизы'); ?>	" class="buttom">На сайт франшизы</a>
		</div>
	</section>
</main>


<?php
get_footer()
?>