<?php
/*
Template Name: О компании
*/
get_header()
?>

<main>
	<ul class="breadcrumbs">
		<li><a href="/"> Главная</a></li>
		<li>О компании</li>
	</ul>

	<section class="chio">
		<div class="container">
			<div class="chio__wrapper">
				<div class="chio__image">
					<img src="		<?php the_field('первое_фото'); ?>	" alt="" />
				</div>
				<div class="chio__image">
					<img src="		<?php the_field('второе_фото'); ?>	" alt="" />
				</div>
				<div class="chio__image">
					<img src="		<?php the_field('третье_фото'); ?>	" alt="" />
				</div>
			</div>
		</div>
	</section>

	<section class="promotion-description">
		<div class="container">
			<h2 class="section-name chio">Японские парикмахерские Чио Чио</h2>
			<p class="promotion-description__sale">
					<?php the_field('описание'); ?>		
			</p>
		</div>
	</section>

	<section class="statistics statistics-chio">
		<div class="container">
			<div class="statistics__wrapper">
				<div class="statistics__place">
					<div class="statistics__place-open">
						<div class="statistics__place-open__img">
							<img src="/wp-content/themes/chio/assets/images/chair.svg" alt="" />
						</div>
						<h3 class="statistics__place-open__text">
							<span>807</span> <br />
							точек
						</h3>
					</div>
					<h3 class="statistics__place-text">
						<span>643</span><br />
						рабочие точки <br />
						и 164 на стадии <br />
						открытия
					</h3>
				</div>

				<div class="statistics__employees">
					<h3 class="statistics__employees-text">
						<span>57%</span><br />франчайзи <br />
						отрывают <br />
						вторые точки
					</h3>

					<div class="statistics__employees-img">
						<img src="/wp-content/themes/chio/assets/images/scissors.svg" alt="" />
					</div>
					<h3 class="statistics__employees-subtext">
						<span>2345</span><br />парикмахеров
					</h3>
				</div>

				<h3 class="statistics__clients">
					<span>2 700 000</span><br />постоянныйх клиентов
				</h3>
			</div>
		</div>
	</section>
</main>



<?php
get_footer()
?>