<?php

/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package chio
 */

?>
<!doctype html>
<html <?php language_attributes(); ?>>

<head>
	<meta charset="<?php bloginfo('charset'); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="https://gmpg.org/xfn/11">

	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
	<?php wp_body_open(); ?>
	<header class="header">
		<div class="container">
			<div class="header__wrap">
				<div class="header__wrap-top">
					<div class="header__wrap-top__city">
						<img src="/wp-content/themes/chio/assets/images/location.svg" alt="location" />
						<p class="header__wrap-top__city-name">Москва</p>
					</div>

					<nav class="header__navigation">
						<?php
						wp_nav_menu(array(
							'theme_location' => 'primary',
							'menu_class' => 'header__wrap-top__navigation-list',
						));
						?>
					</nav>

					<a class="header__wrap-top__phone" href="tel:88006006516">8 800 600-65-16</a>
				</div>

				<div class="header__wrap-middle">
					<div class="logo">

						<a href="/"><img src="	<?php echo esc_url(wp_get_attachment_image_src(get_theme_mod('custom_logo'), 'full')[0]); ?>" alt="logo" /></a>
					</div>
					<div class="search">
						<?php get_search_form(); ?>
					</div>
					<p class="singUp">Личный кабинет</p>
				</div>

				<div class="header__wrap-down">
					<?php
					wp_nav_menu(array(
						'theme_location' => 'header_wrap_down',
						'menu_class' => 'header__wrap-down-navigation',
					));
					?>

				</div>
			</div>
		</div>
	</header>

	<header class="header-mobail">
		<div class="container">
			<div class="header-mobail__wrapper">
				<img class="header-mobail__logo" src="/wp-content/themes/chio/assets/images/logo.svg" alt="logo" />
				<div class="header-mobail__optional">
					<img class="login" src="/wp-content/themes/chio/assets/images/log.svg" alt="войти" />
					<img class="city" src="/wp-content/themes/chio/assets//images/location.svg" alt="город" />
					<div class="burger-menu">
						<span></span>
					</div>
				</div>
			</div>
		</div>
	</header>

	<nav class="navigation-mobail">
		<ul class="navigation-mobail__list">
			<li>
				<p class="navigation-mobail__li">Услуги</p>
				<?php
				wp_nav_menu(array(
					'theme_location' => 'header_wrap_down',
					'menu_class' => 'navigation-mobail__list-two',
				));
				?>

			</li>
			<?php
			wp_nav_menu(array(
				'theme_location' => 'primary',
				'menu_class' => '',
			));
			?>
		</ul>
		<button class="btn">Войти</button>
	</nav>